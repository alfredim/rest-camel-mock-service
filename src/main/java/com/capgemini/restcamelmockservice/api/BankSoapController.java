package com.capgemini.restcamelmockservice.api;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static com.capgemini.restcamelmockservice.constants.Constants.BANK_ROUTE;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * This should work as a controller to retrieve Bank Sort Codes from SOAP endpoint
 */
@Component
public class BankSoapController extends RouteBuilder {

    private static final String BANK_CONTROLLER_ROUTE_ID = "Bank controller route";

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .component("servlet");

        rest()
                .id(BANK_CONTROLLER_ROUTE_ID)
                .get("/bank")
                //.consumes(String.valueOf(APPLICATION_XML))
                .produces(String.valueOf(APPLICATION_JSON))
                .to(BANK_ROUTE);

    }
}
