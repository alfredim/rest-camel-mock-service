package com.capgemini.restcamelmockservice.api;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static com.capgemini.restcamelmockservice.route.HelloWorldRoute.HELLO_ROUTE;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Component
public class HelloWorldController extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        restConfiguration().component("servlet");

        rest()
                .id("Configuration route")
                .get("/hello")
                .produces(String.valueOf(APPLICATION_JSON))
                .to(HELLO_ROUTE);

    }
}
