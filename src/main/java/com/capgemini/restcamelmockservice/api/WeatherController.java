package com.capgemini.restcamelmockservice.api;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static com.capgemini.restcamelmockservice.route.WeatherRoute.WEATHER_ROUTE;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * This should work as a controller to retrieve weather information
 */
@Component
public class WeatherController extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .component("servlet");

        rest()
                .id("Weather controller route")
                .get("/weather")
                .consumes(String.valueOf(APPLICATION_JSON))
                .produces(String.valueOf(APPLICATION_JSON))
                .to(WEATHER_ROUTE);
    }
}
