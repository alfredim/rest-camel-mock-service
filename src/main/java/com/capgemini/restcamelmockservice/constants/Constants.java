package com.capgemini.restcamelmockservice.constants;

public class Constants {

    private Constants() {
    }

    public static final String KEY_HEADER_KEY = "x-rapidapi-key";
    public static final String KEY_HEADER_VALUE = "954bbabe91msh1c389b9600ee65fp130954jsn171fdd184fe7";

    public static final String HOST_HEADER_KEY = "x-rapidapi-host";
    public static final String HOST_HEADER_VALUE = "community-open-weather-map.p.rapidapi.com";

    public static final String WEATHER_API_ENDPOINT = "https://community-open-weather-map.p.rapidapi.com/weather";

    public static final String BANK_ROUTE = "direct:get-bank-codes";
    public static final String BANK_ROUTE_ID = "Retrieving bank codes route";
    public static final String BANK_ENDPOINT = "http://www.thomas-bayer.com/axis2/services/BLZService";
}
