package com.capgemini.restcamelmockservice.model;

import lombok.Data;

@Data
public class BankCodesModel {

    private String BIC;

    private String city;
}
