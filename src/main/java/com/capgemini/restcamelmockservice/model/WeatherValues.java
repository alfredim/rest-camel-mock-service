package com.capgemini.restcamelmockservice.model;

import lombok.Data;

@Data
public class WeatherValues {

    private String temperature;

    private String humidity;
}
