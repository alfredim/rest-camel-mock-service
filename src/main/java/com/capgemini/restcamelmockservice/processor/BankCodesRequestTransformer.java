package com.capgemini.restcamelmockservice.processor;

import com.thomas_bayer.blz.GetBankType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class BankCodesRequestTransformer {

    private static final Logger log = LoggerFactory.getLogger(BankCodesRequestTransformer.class);

    public String transformRequest() {
        GetBankType requestBody = new GetBankType();
        requestBody.setBlz("37010050");

        String body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:blz=\"http://thomas-bayer.com/blz/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <blz:getBank>\n" +
                "         <blz:blz>37010050</blz:blz>\n" +
                "      </blz:getBank>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        log.info("*****************************************************");
        log.info("Request body : {}", requestBody.toString());
        return body;
    }
}
