package com.capgemini.restcamelmockservice.processor;

import com.capgemini.restcamelmockservice.model.BankCodesModel;
import com.thomas_bayer.blz.GetBankResponseType;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class BankCodesResponseProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(BankCodesResponseProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        log.info("Exchange body : {}", exchange.getIn().getBody(String.class) );
        GetBankResponseType soapResponse = exchange.getIn().getBody(GetBankResponseType.class);

        log.info("************************************************************+++++");
        log.info("Soap responde : {}", soapResponse);
        BankCodesModel body = new BankCodesModel();
        body.setBIC(soapResponse.getDetails().getBic());
        body.setCity(soapResponse.getDetails().getOrt());

        exchange.getOut().setBody(body);
    }
}
