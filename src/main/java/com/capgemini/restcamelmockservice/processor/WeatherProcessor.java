package com.capgemini.restcamelmockservice.processor;

import com.capgemini.restcamelmockservice.model.WeatherValues;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class WeatherProcessor implements Processor {

    private ObjectMapper objectMapper;

    @Autowired
    public WeatherProcessor(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void process(Exchange exchange) throws Exception {

        JsonNode response = objectMapper.readValue(exchange.getIn().getBody(String.class), JsonNode.class);
        WeatherValues dto = new WeatherValues();

        dto.setTemperature(response.at("/main/temp").asText(""));
        dto.setHumidity(response.at("/main/humidity").asText(""));

        exchange.getOut().setHeader(Exchange.CONTENT_TYPE, MediaType.APPLICATION_JSON);
        exchange.getOut().setBody(objectMapper.writeValueAsString(dto));
    }
}
