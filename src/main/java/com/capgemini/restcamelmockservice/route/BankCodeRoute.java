package com.capgemini.restcamelmockservice.route;

import com.capgemini.restcamelmockservice.processor.BankCodesRequestTransformer;
import com.capgemini.restcamelmockservice.processor.BankCodesResponseProcessor;
import com.thomas_bayer.blz.GetBankResponseType;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.spring.ws.SpringWebserviceConstants;
import org.apache.camel.model.dataformat.JaxbDataFormat;
import org.apache.commons.lang3.ClassUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import static com.capgemini.restcamelmockservice.constants.Constants.*;

@Component
public class BankCodeRoute extends RouteBuilder {

    private BankCodesResponseProcessor bankCodesResponseProcessor;

    @Autowired
    public BankCodeRoute(BankCodesResponseProcessor bankCodesResponseProcessor) {
        this.bankCodesResponseProcessor = bankCodesResponseProcessor;
    }

    @Override
    public void configure() throws Exception {

        from(BANK_ROUTE)
                .id(BANK_ROUTE_ID)
                .bean(BankCodesRequestTransformer.class)
                //.setHeader(SpringWebserviceConstants.SPRING_WS_SOAP_ACTION, constant(BANK_ENDPOINT))
                .setHeader("Content-Type", constant("text/xml"))
                .setHeader("SOAPAction", constant("<SOAP Action>"))
                .log("sent[headers]: ${headers}, sent[body]: ${body}")

                .to(BANK_ENDPOINT + "?bridgeEndpoint=true")
                .unmarshal(getJaxbFormat(GetBankResponseType.class))
                .process(bankCodesResponseProcessor);
    }

    private JaxbDataFormat getJaxbFormat(Class clazz) {
        JaxbDataFormat jaxbDataFormat = new JaxbDataFormat();
        jaxbDataFormat.setContextPath(ClassUtils.getPackageCanonicalName(clazz));
        return jaxbDataFormat;
    }
}
