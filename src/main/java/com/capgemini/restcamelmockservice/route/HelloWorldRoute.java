package com.capgemini.restcamelmockservice.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldRoute extends RouteBuilder {

    public static final String HELLO_ROUTE = "direct:hello";

    @Override
    public void configure() throws Exception {

        from(HELLO_ROUTE)
                .routeId("Hello route")
                .transform().simple("Hello world!")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));
    }
}
