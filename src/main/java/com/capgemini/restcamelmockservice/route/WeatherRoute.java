package com.capgemini.restcamelmockservice.route;

import com.capgemini.restcamelmockservice.processor.WeatherProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.capgemini.restcamelmockservice.constants.Constants.WEATHER_API_ENDPOINT;

@Component
public class WeatherRoute extends RouteBuilder {

    public static final String WEATHER_ROUTE = "direct:get-weather";

    private WeatherProcessor weatherProcessor;

    @Autowired
    public WeatherRoute(WeatherProcessor weatherProcessor) {
        this.weatherProcessor = weatherProcessor;
    }

    @Override
    public void configure() throws Exception {
        from(WEATHER_ROUTE)
                .routeId("Weather info route")
                .setHeader("x-rapidapi-key", constant("954bbabe91msh1c389b9600ee65fp130954jsn171fdd184fe7"))
                .to(WEATHER_API_ENDPOINT + "?bridgeEndpoint=true&?q=Malmö,se")
                .process(weatherProcessor);
    }
}
